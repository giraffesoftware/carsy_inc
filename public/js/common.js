$(document).on('click', '#open-block-btn', function (e) {
    e.preventDefault();
    var inputsBlock = $('#dynamic-inputs');
    var dynamicField = $('#inputs-block').clone();
    var inputs = inputsBlock.find('.dynamic-input');
    var lastInput = inputs.last();
    if (inputs.length) {
        var name = lastInput.find('.name').val();
        var value = lastInput.find('.value').val();
        if (name !== undefined &&  value !== undefined && name !== '' &&  value !== '') {
            dynamicField.removeAttr('id');
            dynamicField.find('input.name').attr('name', 'dynamicFields[create][' + inputs.length + '][name]');
            dynamicField.find('input.value').attr('name', 'dynamicFields[create][' + inputs.length + '][value]');
            inputsBlock.append(dynamicField.attr('style', ''));
        }
    } else {
        dynamicField.removeAttr('id');
        dynamicField.find('input.name').attr('name', 'dynamicFields[create][' + inputs.length + '][name]');
        dynamicField.find('input.value').attr('name', 'dynamicFields[create][' + inputs.length + '][value]');
        inputsBlock.append(dynamicField.attr('style', ''));
    }

});
