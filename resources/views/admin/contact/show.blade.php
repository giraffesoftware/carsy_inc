@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-heading">Contact {{ $contact->id }}</div>
                    <div class="panel-body">

                        <a href="{{ url('/admin/contact') }}" title="Back">
                            <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i>
                                Back
                            </button>
                        </a>
                        <a href="{{ url('/admin/contact/' . $contact->id . '/edit') }}" title="Edit Contact">
                            <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o"
                                                                      aria-hidden="true"></i> Edit
                            </button>
                        </a>

                        <form method="POST" action="{{ url('admin/contact' . '/' . $contact->id) }}"
                              accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-xs" title="Delete Contact"
                                    onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o"
                                                                                             aria-hidden="true"></i>
                                Delete
                            </button>
                        </form>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tbody>
                                <tr>
                                    <th>ID</th>
                                    <td>{{ $contact->id }}</td>
                                </tr>
                                <tr>
                                    <th> First Name</th>
                                    <td> {{ $contact->first_name }} </td>
                                </tr>
                                <tr>
                                    <th> Last Name</th>
                                    <td> {{ $contact->last_name }} </td>
                                </tr>
                                <tr>
                                    <th> Phone</th>
                                    <td> {{ $contact->phone }} </td>
                                </tr>
                                <tr>
                                    <th> Address</th>
                                    <td> {{ $contact->address }} </td>
                                </tr>
                                @foreach($contact->dynamicContactFields as $dynamicContactField)
                                <tr>
                                    <th> {{ $dynamicContactField->name }}</th>
                                    <td> {{ $dynamicContactField->value }} </td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
