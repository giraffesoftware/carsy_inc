<div class="form-group {{ $errors->has('first_name') ? 'has-error' : ''}}">
    <label for="first_name" class="col-md-4 control-label">{{ 'First Name' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="first_name" type="text" id="first_name"
               value="{{ $contact->first_name or ''}}" required>
        {!! $errors->first('first_name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('last_name') ? 'has-error' : ''}}">
    <label for="last_name" class="col-md-4 control-label">{{ 'Last Name' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="last_name" type="text" id="last_name" value="{{ $contact->last_name or ''}}"
               required>
        {!! $errors->first('last_name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('phone') ? 'has-error' : ''}}">
    <label for="phone" class="col-md-4 control-label">{{ 'Phone' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="phone" type="text" id="phone" value="{{ $contact->phone or ''}}">
        {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
    <label for="email" class="col-md-4 control-label">{{ 'Email' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="email" type="text" id="email" value="{{ $contact->email or ''}}" required>
        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('address') ? 'has-error' : ''}}">
    <label for="address" class="col-md-4 control-label">{{ 'Address' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="address" type="text" id="address" value="{{ $contact->address or ''}}">
        {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
    </div>
</div>

@if (isset($contact) && count($contact->dynamicContactFields))
    @foreach($contact->dynamicContactFields as $key => $dynamicContactField)
        <div class="form-group {{ $errors->has('address') ? 'has-error' : ''}}">

            <label for="address" class="col-md-4 control-label">{{ $dynamicContactField->name }}</label>
            <div class="col-md-6">
                <input type="hidden" name="dynamicFields[update][{{ $key }}][id]"
                       value="{{ $dynamicContactField->id }}">
                <input class="form-control" name="dynamicFields[update][{{ $key }}][value]" type="text"
                       value="{{ $dynamicContactField->value}}">
                {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

    @endforeach
@endif

<div id="dynamic-inputs"></div>




<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        <button class="btn btn-success" type="button" id='open-block-btn'>New field</button>
        <input class="btn btn-primary" type="submit" value="{{ $submitButtonText or 'Create' }}">
    </div>
</div>


<div class="form-group dynamic-input" id='inputs-block' style="display: none">
    <div class="col-md-4">
        <input class="form-control name" name="" placeholder="Field name">
    </div>
    <div class="col-md-6">
        <input class="form-control value" name="" placeholder="Value">
    </div>
</div>