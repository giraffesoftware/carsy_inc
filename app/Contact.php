<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable = [
        'id',
        'user_id',
        'first_name',
        'last_name',
        'phone',
        'email',
        'address',
        'deleted_at'
    ];

    protected $table = 'contacts';

    public function dynamicContactFields()
    {
        return $this->hasMany(DynamicContactField::class);
    }

}
