<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DynamicContactField extends Model
{
    protected $fillable = [
        'id',
        'contact_id',
        'name',
        'value'
    ];

    protected $table = 'dynamic_contact_fields';
}
