<?php

namespace App\Http\Controllers\Admin;

use App\DynamicContactField;
use App\Http\Controllers\Controller;

use App\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use \Illuminate\Support\Facades\Session;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $contact = Contact::where('first_name', 'LIKE', "%$keyword%")
				->orWhere('last_name', 'LIKE', "%$keyword%")
				->orWhere('phone', 'LIKE', "%$keyword%")
				->orWhere('email', 'LIKE', "%$keyword%")
				->orWhere('address', 'LIKE', "%$keyword%")
				->paginate($perPage);
        } else {
            $contact = Contact::paginate($perPage);
        }

        return view('admin.contact.index', compact('contact'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.contact.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'first_name' => 'required|min:3',
			'last_name' => 'required|min:3',
			'email' => 'email'
		]);
        $requestData = $request->all();
        $requestData['user_id'] = Auth::id();
        DB::transaction(function () use ($requestData) {
            /** @var Contact $contact */
            $contact = Contact::create($requestData);
            if (isset($requestData['dynamicFields']['create'])) {
                $this->createDynamicFields($requestData['dynamicFields']['create'], $contact);
            }
        });

        Session::flash('flash_message', 'Contact added!');

        return redirect('admin/contact');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $contact = Contact::findOrFail($id);
        return view('admin.contact.show', compact('contact'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $contact = Contact::findOrFail($id);
        return view('admin.contact.edit', compact('contact'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
			'first_name' => 'required|min:3',
			'last_name' => 'required|min:3',
			'email' => 'email'
		]);
        $requestData = $request->all();
        /** @var Contact $contact */
        $contact = Contact::findOrFail($id);
        DB::transaction(function () use ($requestData, $contact) {
            $contact->update($requestData);
            if (isset($requestData['dynamicFields']['update'])) {
                $this->updateDynamicFields($requestData['dynamicFields']['update']);
            }
            if (isset($requestData['dynamicFields']['create'])) {
                $this->createDynamicFields($requestData['dynamicFields']['create'], $contact);
            }
        });

        Session::flash('flash_message', 'Contact updated!');

        return redirect('admin/contact');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Contact::destroy($id);

        Session::flash('flash_message', 'Contact deleted!');

        return redirect('admin/contact');
    }


    /**
     * Method updates dynamic fields
     *
     * @param array $dynamicFields
     */
    protected function updateDynamicFields(array $dynamicFields)
    {
        foreach ($dynamicFields as $dynamicField) {
            if ($dynamicField['id'] && $dynamicField['value']) {
                $dynamicFieldModel = DynamicContactField::find($dynamicField['id']);
                $dynamicFieldModel->value = $dynamicField['value'];
                $dynamicFieldModel->save();
            }
        }
    }


    /**
     * Method updates dynamic fields
     *
     * @param array $dynamicFields
     * @param Contact $contact
     */
    protected function createDynamicFields(array $dynamicFields, Contact $contact)
    {
        foreach ($dynamicFields as $dynamicField) {
            if ($dynamicField['name'] && $dynamicField['value']) {
                $contact->dynamicContactFields()->create(
                    [
                        'name' => $dynamicField['name'],
                        'value' => $dynamicField['value']
                    ]
                );
            }
        }
    }
}
